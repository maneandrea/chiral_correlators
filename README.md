Chiral correlator
====

Python tool for computing correlators of Virasoro descendants of the vacuum module in 2d chiral CFTs

###  Requisites

1. python >=3.8
2. sympy

### Features

For now it only computes two- and three-point functions. Implementing four-point functions is easy and it might be done eventually.

### Usage

 - `two_pf(m, k)` takes two tuples `m=(m1,m2,...)` and `k=(k1,k2,...)` representing the two-point function `<(L_m1L_m2...ψ)(L_k1L_k2...ψ)>` where ψ is the vacuum state.
 - `three_pf(m, k, l)` takes three tuples `m`, `k` and `l` representing the three-point function `<(L_m1L_m2...ψ)(L_k1L_k2...ψ)(L_l1L_l2...ψ)>` where ψ is the vacuum state.
 - `two_pf_op(which_z_ij, T_left, T_right)` represents a two-point function of a linear combination of the Virasoro descendants, which is given by the class `L`
 - `L` is a class that contains a `dict` of the form `{(k1,k2,...): coeff, ...}` representing a linear combination `coeff * L_k1L_k2...`
 - `ZPower` is a class that contains a `dict` of the form `{(p1,p2,p3): coeff, ...}` representing a linear combination `coeff * z_ij^p1 z_ik^p2 z_jk^p3`

You can also feed a file to this script and it will return the same file with the two- or three-point functions computed. The syntax is

```
twoPFpy[z[i_,j_]][m1,m2,...][k1,k2,...]
threePFpy[z[i_,j_],z[i_,k_],z[j_,k_]][m1,m2,...][k1,k2,...][l1,l2,...]
```

The result can be imported in Mathematica.


#!/usr/bin/env python
import math
import sympy
from sympy import mathematica_code
import argparse
import os
import re

c = sympy.symbols('c')


def merge(A, B, f):
    """Merge dictionaries from https://stackoverflow.com/a/16560975/12008379"""
    # Start with symmetric difference; keys either in A or B, but not both
    merged = {k: A.get(k, B.get(k)) for k in A.keys() ^ B.keys()}
    # Update with `f()` applied to the intersection
    merged.update({k: f(A[k], B[k]) for k in A.keys() & B.keys()})
    return merged


def plus(*a):
    return sum(a)


class LinCom:
    """Arbitrary combination of objects times numerical coefficients"""
    def __init__(self, z_dict):
        self.z_dict = z_dict

    def __add__(self, other):
        """Adds two prefactors by merging their dicts"""
        if isinstance(other, LinCom):
            new_z_dict = merge(self.z_dict, other.z_dict, plus)
        elif isinstance(other, int):
            new_z_dict = merge(self.z_dict, {(): other}, plus)
        else:
            raise NotImplemented

        return self.__class__(new_z_dict)

    def __mul__(self, other):
        """Multiplies a prefactor by an expression"""
        new_z_dict = {key: val*other for key, val in self.z_dict.items()}

        return self.__class__(new_z_dict)

    def __rmul__(self, other):
        return self.__mul__(other)


class L(LinCom):
    """The objects of the linear combination are L[k1,k2,...]"""
    def __init__(self, args):
        super().__init__(args)

    def __xor__(self, other):
        """Multiply by another L on its right"""
        if isinstance(other, tuple):
            new_z_dict = {tuple(list(key) + list(other)): val for key, val in self.z_dict.items()}
        else:
            raise NotImplemented

        return L(new_z_dict)

    def __rxor__(self, other):
        """Multiply by another L on its left"""
        if isinstance(other, tuple):
            new_z_dict = {tuple(list(other) + list(key)): val for key, val in self.z_dict.items()}
        else:
            raise NotImplemented

        return L(new_z_dict)

    def stress_tensor_form(self):
        """Makes the L's in the form () or (...,-2) *in place*"""
        new_z_dict = {}
        for ks, coeff in self.z_dict.items():
            if len(ks) > 0:
                if (n := ks[-1]) < -1:
                    new_ks = ks[:-1] + (-n - 2) * (-1,) + (-2,)
                    new_coeff = coeff * sympy.sympify(f'1/({-n-2})!')
                    new_z_dict[new_ks] = new_coeff
            else:
                new_z_dict[()] = coeff

        self.z_dict = new_z_dict

    def __repr__(self):
        result = []
        for ell, coeff in self.z_dict.items():
            result.append(str(coeff) + '*L[' + ','.join(str(a) for a in ell) + ']')
        return '+'.join(result)


class ZPower(LinCom):
    """The objects of the linear combination are powers of z. The powers are stored as (z_ij, z_ik, z_jk)"""
    def __init__(self, args):
        super().__init__(args)

    def __xor__(self, other):
        """Multiply by another z monomial on its right"""
        if isinstance(other, tuple):
            new_z_dict = {tuple([a + b for a, b in zip(key, other)]): val for key, val in self.z_dict.items()}
        else:
            raise NotImplemented

        return ZPower(new_z_dict)

    def __rxor__(self, other):
        return self.__xor__(other)

    def __repr__(self):
        result = []
        for tup, coeff in self.z_dict.items():
            zijstr = f'z[i,j]^{tup[0]}' if tup[0] not in (0, 1) else {0: '', 1: 'z[i,j]'}[tup[0]]
            zikstr = f'z[i,k]^{tup[1]}' if tup[1] not in (0, 1) else {0: '', 1: 'z[i,k]'}[tup[1]]
            zklstr = f'z[j,k]^{tup[2]}' if tup[2] not in (0, 1) else {0: '', 1: 'z[j,k]'}[tup[2]]
            result.append('(' + mathematica_code(coeff) + ')*' + zijstr + zikstr + zklstr)
        return '+'.join(result)


def binom(a, b):
    try:
        return math.comb(a, b)
    except ValueError:
        return 1 if b == 0 else 0


def mode(mlist, n, acts_on):
    """Mode expansion of Virasoro descendants of the vacuum module"""
    if len(mlist) == 0:
        return L({(n,): 1})
    if len(mlist) == 1:
        m = - mlist[0]
        if n + acts_on > 0:
            return L({(): 0})
        else:
            return L({
                (p, n-p): binom(-p-2, -p-m) for p in range(acts_on+n, -m+1)
            }) + L({
                (n-p, p): (-1)**m * binom(p+m-1, p+1) for p in range(-1, -acts_on+1)
            })
    else:
        m = -mlist[0]
        mrest = mlist[1:]
        if n + acts_on > 0:
            return L({(): 0})
        else:
            p = acts_on + n
            current = binom(-p-2, -p-m) * ((p,) ^ mode(mrest, n-p, acts_on))
            for p in range(acts_on + n + 1, -m + 1):
                current += binom(-p-2, -p-m) * ((p,) ^ mode(mrest, n-p, acts_on))

            p = -1
            current += (-1)**m * binom(p + m - 1, p + 1) * (mode(mrest, n - p, acts_on + p) ^ (p,))
            for p in range(0, -acts_on + 1):
                current += (-1)**m * binom(p + m - 1, p + 1) * (mode(mrest, n - p, acts_on + p) ^ (p,))

            return current


def reduce_dot(sum_of_L):
    """Reduces the product of L's acting on a state of holomorphic dimension h=0"""

    def rule_applies(k_list):
        if len(k_list) == 0:
            return ()
        else:
            for pos in range(len(k_list) - 1):
                if k_list[pos] > k_list[pos+1]:
                    return pos, pos + 1
            return ()

    to_return = L(sum_of_L.z_dict.copy())

    for l, val in list(to_return.z_dict.items()):
        if val == 0:
            del to_return.z_dict[l]

    changed_something = True

    while changed_something:
        changed_something = False

        for l in list(to_return.z_dict.keys()):
            if to_fix := rule_applies(l):
                changed_something = True
                n, m = to_fix

                l_flipped = l[:n] + (l[m], l[n]) + l[m+1:]
                l_sum = l[:n] + (l[n] + l[m],) + l[m+1:]

                coeff = to_return.z_dict[l]
                temp_to_return = {
                    l_flipped: coeff,
                    l_sum: coeff * (l[n] - l[m])
                }

                if l[n] + l[m] == 0:
                    l_central = l[:n] + l[m + 1:]
                    temp_to_return[l_central] = coeff * c / 12 * l[n] * (l[n] ** 2 - 1)

                del to_return.z_dict[l]
                to_return += L(temp_to_return)

            elif l and l[-1] >= -1:
                changed_something = True
                del to_return.z_dict[l]

    return to_return


def two_pf(m, k):
    """Implements twoPF defined in Mathematica"""
    orderk = sum(k)

    expr = reduce_dot(
        mode(m, -orderk+2, orderk-2) ^ (*k, -2)
    ).z_dict[()]
    return sympy.factor(expr)


def two_pf_op(which_z_ij, T_left, T_right, offset=(0, 0, 0)):
    """Implements the two-point function for linear combinations of T's
    which_z_ij = (1,0,0) for z_12, (0,1,0) for z_13 and (0,0,1) for z_23
    same for offset that can add another overall factor of z_ij"""
    new_z_dict = {}

    for key_left, coeff_left in T_left.z_dict.items():
        for key_right, coeff_right in T_right.z_dict.items():
            if key_left == () and key_right == ():
                new_z_dict[offset] = new_z_dict.get(offset, 0) + coeff_left * coeff_right
            elif len(key_left) > 0 and len(key_right) > 0:
                # WARNING: if the entries are not in the stress tensor form this is wrong!
                new_left = key_left[:-1]
                new_right = key_right[:-1]
                power = sum(key_left) + sum(key_right)
                entry = tuple(a*power + off for a, off in zip(which_z_ij, offset))
                new_z_dict[entry] = new_z_dict.get(entry, 0) + coeff_left * coeff_right * two_pf(new_left, new_right)

    return ZPower(new_z_dict)


def three_pf(m, k, l):
    """Implements threePF defined in Mathematica"""
    orderm = sum(m)
    orderk = sum(k)
    orderl = sum(l)

    # Singular OPE 12
    sum_z = ZPower({})
    for nnn in range(1, -orderm-orderk+5):
        ope_left = reduce_dot(mode(m, nnn+orderm-2, orderk-2) ^ (*k, -2))
        ope_left.stress_tensor_form()
        sum_z += two_pf_op((0, 0, 1), ope_left, L({tuple(l)+(-2,): 1}), offset=(-nnn, 0, 0))

    # Singular OPE 13
    for nnn in range(1, -orderm-orderl+5):
        ope_right = reduce_dot(mode(m, nnn+orderm-2, orderl-2) ^ (*l, -2))
        ope_right.stress_tensor_form()
        sum_z += two_pf_op((0, 0, 1), L({tuple(k)+(-2,): 1}), ope_right, offset=(0, -nnn, 0))

    return sum_z


def main():
    """When launched from terminal"""
    parser = argparse.ArgumentParser(description='Takes a file of two- or three-point functions and computes them')
    parser.add_argument('file', type=str, help='the file from which to take input')
    parser.add_argument('out', type=str, help='the file to output (default=FILE_out.m)', default=None, nargs='?')
    parser.add_argument('-c', dest='cval', type=str, help='the value of c (default is symbolic)', default=None)

    args = parser.parse_args()

    if args.cval is not None:
        global c
        c = sympy.sympify(args.cval)

    if not os.path.isfile(args.file):
        print(f'file {args.file} does not exist')
        raise FileNotFoundError

    if args.out is None:
        name, ext = os.path.splitext(args.file)
        args.out = name + '_out' + ext

    total = sum(1 for line in open(args.file, 'r'))
    progress = 0

    # Reads from a file the lines twoPF[m1,m2,...][k1,k2,...]
    # and writes another file with twoPF[m1,m2,...][k1,k2,...] = <result>;
    # with <result> being written in Mathematica format
    with open(args.file, 'r') as f_read:
        with open(args.out, 'w+') as f_write:
            for feed in f_read:
                feed_line = feed.strip(' \n')
                three_pf_match = re.match(
                    r'threePFpy\[z\[i_,j_],z\[i_,k_],z\[j_,k_]]\[([-\d,]*)]\[([-\d,]*)]\[([-\d,]*)]',
                    feed_line
                )
                if three_pf_match:
                    m_temp = three_pf_match.group(1)
                    k_temp = three_pf_match.group(2)
                    l_temp = three_pf_match.group(3)

                    m = [int(a) for a in m_temp.split(',') if len(a) > 0]
                    k = [int(a) for a in k_temp.split(',') if len(a) > 0]
                    l = [int(a) for a in l_temp.split(',') if len(a) > 0]

                    result = three_pf(m, k, l)

                    f_write.write(feed_line + ' := ' + result.__repr__() + '\n')

                else:
                    two_pf_match = re.match(
                        r'twoPFpy\[z\[i_,j_]]\[([-\d,]*)]\[([-\d,]*)]',
                        feed_line
                    )
                    if two_pf_match:
                        m_temp = two_pf_match.group(1)
                        k_temp = two_pf_match.group(2)

                        m = [int(a) for a in m_temp.split(',') if len(a) > 0]
                        k = [int(a) for a in k_temp.split(',') if len(a) > 0]

                        dim = sum(m) + sum(k) - 4
                        result = ZPower({(dim, 0, 0): two_pf(m, k)})

                        f_write.write(feed_line + ' := ' + result.__repr__() + '\n')

                progress += 1
                print(f'\rdone: {progress} of {total}', end='')

    print('')


if __name__ == '__main__':
    main()
